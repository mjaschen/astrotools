Changelog
=========

All notable changes to `mjaschen/astrotools` will be documented in this file.
Updates should follow the [Keep a CHANGELOG](http://keepachangelog.com/) principles.
This project adheres to [Semantic Versioning](http://semver.org/).

## [0.0.2] - 2016-02-08

### Fixed

* changed algorithm for Julian Day to calendar date conversion

## [0.0.1] - 2015-03-06

### Added

* add classes for Julian Day, Sidereal Time, and Date of Easter calculations
